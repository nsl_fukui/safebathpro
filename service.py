#!/home/pi/.pyenv/shims/python
# -*- coding: utf-8 -*-


import sys
import os
import json
import argparse
import dbus.mainloop.glib
from gi.repository import GLib
from bluez import *
from blp import BloodPressureFeature, BloodPressureMeasurement, BinaryReader
from selector import scan_device, select_device

mainloop = GLib.MainLoop()


class BlookPressureService:
    def __init__(self, device):
        self.device = device
        self.measurements = []

        dbus.SystemBus().add_signal_receiver(self.device_changed_cb,
                                             dbus_interface=INTERFACE.PROPERTIES,
                                             signal_name='PropertiesChanged',
                                             arg0=Device.INTERFACE,
                                             path_keyword='path')

    def device_changed_cb(self, interface, changed, invalidated, path):
        if changed.get('ServicesResolved'):
            print('ServicesResolved')
            self.init_measurement_indication()

    def get_device_information(self, targets=None):
        if not targets:
            targets = (CHARACTERISTIC.MODEL_NUMBER_STRING,
                       CHARACTERISTIC.SERIAL_NUMBER_STRING,
                       CHARACTERISTIC.FIRMWARE_REVISION_STRING,
                       CHARACTERISTIC.HARDWARE_REVISION_STRING,
                       CHARACTERISTIC.SOFTWARE_REVISION_STRING,
                       CHARACTERISTIC.MANUFACTURER_NAME_STRING,
                       )
        if not isinstance(targets, (list, tuple)):
            targets = tuple(targets)

        service = self.device.find_service(SERVICE.DEVICE_INFORMATION)
        return dict((uuid, BinaryReader(service.find_characteristic(uuid).ReadValue({})).utf8s())
                    for uuid in targets)

    def get_feature(self):
        service = self.device.find_service(SERVICE.BLOOD_PRESSURE)
        characteristic = service.find_characteristic(CHARACTERISTIC.BLOOD_PRESSURE_FEATURE)
        return BloodPressureFeature(BinaryReader(characteristic.ReadValue({})).uint16())

    def init_measurement_indication(self):
        def indication_cb(interface, changed, invalidated):
            if 'Value' in changed:
                self.measurements.append(BloodPressureMeasurement(changed['Value']))

        service = self.device.find_service(SERVICE.BLOOD_PRESSURE)
        characteristic = service.find_characteristic(CHARACTERISTIC.BLOOD_PRESSURE_MEASUREMENT)
        interface = characteristic.get_interface(INTERFACE.PROPERTIES)
        interface.connect_to_signal('PropertiesChanged', indication_cb)
        characteristic.StartNotify()


class Config(dict):
    def __init__(self, filename):
        super().__init__()
        self.filename = filename
        try:
            self.load()
        except IOError:
            pass

    def load(self):
        with open(self.filename) as f:
            self.clear()
            self.update(json.load(f))

    def save(self):
        with open(self.filename, 'w') as f:
            json.dump(self, f, indent=2)


def error_cb(error):
    print('D-Bus call failed: ' + str(error))


def parse_argument(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--init', action='store_true', help='discover device, and setup')
    parser.add_argument('-a', '--adapter', help='Bluetooth adapter device path')
    parser.add_argument('-v', '--verbose', help='Verbose debug message')
    parser.add_argument('address', nargs='?', help='target device address e.g.AA:BB:CC:DD:EE:FF')
    return parser.parse_args(args)


def find_adapter(adapter_name):
    adapters = Adapter.list_adapter()
    if adapter_name:
        for adapter in adapters:
            if adapter.path == adapter_name or adapter.path.endswith('/' + adapter_name):
                break
        else:
            adapter = None
    else:
        adapter = adapters[0] if adapters else None
    return adapter


def main(argv, config_file):
    # setup
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    manager = dbus.Interface(get_object('/'), INTERFACE.OBJECT_MANAGER)
    manager.connect_to_signal('InterfacesRemoved', lambda *args: mainloop.quit)

    # argument & configuration
    args = parse_argument(argv)
    config = Config(config_file)

    # get Bluetooth adapter
    adapter = find_adapter(args.adapter or config.get('adapter'))
    if not adapter:
        print('Bluetooth adapter not found')
        return
    if adapter.path != config.get('adapter'):
        config['adapter'] = adapter.path
        config.save()

    # discover device
    if args.init:
        address = select_device(scan_device(adapter), args.address)
        if not address:
            return
        config['device'] = str(address)
        config.save()
    if not config.get('device'):
        print('Device address not found')
        return

    device = Device.get_device(config['device'])
    bls = BlookPressureService(device)

    # connect device
    print('connect {}'.format(config['device']))
    device.Connect()
    dump(get_managed_info(device.path))

    if not device.Paired:
        print('pairing')
        agent = PairingAgent.register_agent('/org/bluez/xxx/agent')
        device.pairing()

    # main loop
    mainloop.run()

    # TODO: ファイル保存とか configで指定したサービスを呼ぶとか
    for measurement in bls.measurements:
        fs.appendFile('/home/pi/safeblood.csv', req.body.value + '\n',(error) => { #指定したファイルに血圧データを書き込む
        }); 
        measurement.dump()


if __name__ == '__main__':
    main(sys.argv[1:], os.path.splitext(sys.argv[0])[0] + '.cfg')
