# SAFEBATH pro

浴室内でのヒートショックの予防を目的とした、浴室と脱衣所における安全な環境制御システム。
以下の4つの機能を実現することで、環境制御を行なっている。

* 浴室と脱衣所の温度差を5度以内に保つ
* 血圧変動から出浴のタイミングをアドバイスする
* ユーザの血圧にあった湯温に調節する
* 緊急時に家族や医師へ連絡する

## Usage

プロジェクトを複製してローカル端末で2つのコマンドプロンプトにおいて実行し、開発や検証ができるまでの手順を以下に示す。実際のシステムにプロジェクトをデプロイする方法については、デプロイの項目を参照してください。
* コマンドプロンプト1
```
git clone https://gitlab.com/nsl_fukui/safebathpro.git examples
cd examples
python3 service.py
gatttool -b EC:21:E5:xx:xx:xx -t random -I
connect
```

* コマンドプロンプト2
```
node app.js
mesh start
cd examples 
python3 layout.py
```

### Prerequisites

* Python 3.6.6
* Node.js 8.11.1
* Express 4.16.1
* MESH Hub
* Bluez 5.43
* Dbus 1.10.28
* Raspberry Pi 3 Model B+
* 上腕式血圧計 HEM-7600T
* MESHタグ(温度・湿度) ×2
* NatureRemo
* SwitchBot ×2
* SwitchBot Hub
* スピーカー(有線)


### Installing

動作する開発環境の構築方法を段階的に例示する。

* Step 1: 最新のバージョンのNode.jsをインストール
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
* Step 2: Express Generatorをグローバル環境にインストール
```
sudo npm install express-generator -g
```

* Step 3: Raspberry Piのブラウザで[MESHサインインページ](https://hub.meshprj.com/users/sign_in?locale=ja)からアカウントを作成、サインインし、直接パッケージをダウンロード

* Step 4: パッケージが配置できているか確認し、依存モジュールをインストール
```
sudo apt-get update
sudo apt-get install bluez dnsutils sqlite3 libcrypto++-dev libcrypto++-utils libcurl4-openssl-dev
```
* Step 5: MESH Hubアプリケーション本体をインストール (x.x.xにはダウンロードしたパッケージのバージョンを入力)
```
sudo dpkg -i mesh_x.x.x_armhf.deb
```

* Step 6: Bluetoothドライバをインストール
```
sudo apt install bluetooth
```

* Step 7: [BlueZ](http://www.bluez.org)をインストール
```
wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.43.tar.xz
xz -dv bluez-5.43.tar.xz
tar -xf bluez-5.43.tar
cd bluez-5.43/
./configure --enable-experimental
make
sudo make install
sudo pip install pybluez
```

* Step 8: Dbusのインストール
```
python3
import dbus
```

## Running the Examples
プログラムの実行方法を以下に示す。

* コマンドプロンプト1
```
python3 service.py
gatttool -b EC:21:E5:xx:xx:xx -t random -I
connect
```
* コマンドプロンプト2
```
node app.js
mesh start
python3 layout.py
```


### Setting the examples
プログラムの実行結果を以下に示す。

```
湯沸かす前

暖房開始
スイッチオンの状態

通常血圧です

浴室暖房終了

入浴開始

そろそろお湯から出ましょう               
Playing MPEG stream from su698.mp3 ...

出浴した
入浴終了

電源OFF
```


## Author

* **福井 菜央


## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

