# coding: UTF-8 
import requests  #IFTTTのレシピを呼び出すためのモジュール
import csv　     #csvファイルを読み込むためのモジュール
import time      #並列処理を行うためのモジュール
import asyncio   #並列処理を行うためのモジュール
import subprocess#コマンド実行のためのモジュール


bathtemp = 0    #風呂の温度	
differ = 0      #温度差
onoff = 0       #暖房機がオン状態は1 オフ状態0
state = 0       #暖房機のモードが強暖０　弱１　弱暖2
strongorder = 0 #強設定関数
nextcount = 0   #待ち時間用
humancount = 0  #人がいるかいないかの識別関数
nothuman = 0    #人がいなくなった時1
weakorder = 0   #弱設定関数 
gudge=0         #出浴したか


def scan_csv(filepath): #csvファイル読み込み関数
    with open(filepath) as f: #filepathで指定したファイルを開く
        reader = csv.reader(f)
        l = [row for row in reader]
    return l

def ifttt_webhook(eventid): #IFTTTレシピ呼び出す関数
    url = "https://maker.ifttt.com/trigger/" + eventid+ "/with/key/b7tJ4uC22Vm84-xuhGutA4fZLUtdFwVz1SdpWh5Zlqn" #呼び出したいレシピのIDとWebhookにおけるkeyをurlに記入
    response = requests.post(url)
    return

async def water(sec): #並行処理①（血圧によって湯温を変える）
    
    while True:
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, time.sleep, sec) #指定した時間分待って実行

        dataWater=[]　#血圧値を格納する配列
        bloodwater_path = "/home/pi/safeblood.csv" # filepathの指定
        bloodwater = scan_csv(bloodwater_path)　#指定したファイルを読み込む
        num = len(bloodwater) #numにファイルの長さを代入
        
        for i in range(0,num):
            dataWater.append(bloodwater[i][0]) #読み取った値を配列に代入
        
        if i==1:
            blood=int(dataWater[1]) #計測２回目の血圧値の整数型を基準値として代入

            if blood>0 and blood<=115: #基準値>0で基準値<=115のとき
                print("通常血圧です") #41度設定のまま
                break

            if blood>115: #基準値>115のとき
                print("血圧高めなので湯温を下げます")
                ifttt_webhook("dangerwater") #IFTTTのイベントdangerwaterを２回呼び出して湯沸かし機リモコンのボタンを２回押す
                ifttt_webhook("dangerwater")
                break
                
    
    return

async def advice(sec): #並列処理②（出浴タイミングのアドバイスをする）
    global gudge #出浴したかどうかの変数（出浴前0, 出浴後1）
    sound=0 #音声出力でのアドバイスをしたかどうかの変数（アドバイス前0, アドバイス後1）
    
    while True:
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, time.sleep, sec) #指定した時間分待って実行
        
        dataAdvice=[] #血圧値を格納する配列
        bloodadvice_path = "/home/pi/safeblood.csv" # filepathの指定
        bloodadvice = scan_csv(bloodadvice_path) #指定したファイルを読み込む
        num = len(bloodadvice) #numにファイルの長さを代入
               
        for i in range(0,num):
            dataAdvice.append(bloodadvice[i][0]) #読み取った値を配列に代入
        
        if i>1 :
            diff=float(dataAdvice[1])-float(dataAdvice[i]) #基準値-最新値で変動幅を求める
            differ=round(diff,1) #変動幅を四捨五入する
            
            if differ > 8 and sound==0: #変動幅が8以上で音声出力がまだの場合
                cmd="mpg321 su698.mp3" #音声出力するコマンドを代入
                subprocess.Popen(cmd.split()) #コマンド実行
                sound=1 #音声出力済なので1を代入
            elif differ >= 10 and sound==1: #変動幅が10以上で音声出力済               
                ifttt_webhook("notification") #IFTTTのイベントnotificationを呼び出してLINEに通知
                sound=0 #line通知済なのでリセット
                break
            
            
        if gudge==1: #人感センサ触れて出浴確認された場合
            break
    return
        




async def excute(sec): #並行処理③（温度差を調節する）
    global gudge #出浴したかどうかの関数
    global onoff #脱衣所暖房の電源の状態（オフ0, オン1）
    global humancount #人感センサの値（人いない0, 人いる1）
    global nothuman =0 #人がいるかどうか
    global nextcount #カウント
    global ontime #脱衣所暖房機がオンされたかどうか
    global weakorder #脱衣所暖房モード弱設定関数
    global state #脱衣所暖房機のモード(強暖０,弱１,弱暖2)
    global strongorder #脱衣所暖房モード強設定関数
    cnt=0 #カウント
    bathwarm=0 #浴室暖房の電源の状態（オフ0, オン1）
    
    while True:
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, time.sleep, sec) #指定した時間分待って実行
        
        temp_path = "/home/pi/safebathsub.csv" # filepathの指定
        tempdata = scan_csv(temp_path) #指定したファイルを読み込む
        #関数に読み取ったデータ代入
        bathtemp=0 #浴室の温度
        bathtemp1=0 #脱衣所の温度
        temp=0 #float型の浴室の温度
        temp1=0 #float型の脱衣所の温度
        cnt=0 #カウント
        dataTemp_bath=[] #浴室温度の配列
        dataTemp_room=[] #脱衣所温度の配列
        dataTemp_human=[] #人感センサ値の配列
        num = len(tempdata) #読み込んだデータの長さ代入
        
        for i in range(1,num): #読み取った値をそれぞれ配列に代入
            dataTemp_human.append(tempdata[i][2]) 
            dataTemp_room.append(tempdata[i][1]) 
            dataTemp_bath.append(tempdata[i][0])
        
            humancount=int(dataTemp_human[i-1]) #人感センサ値の最新値を整数型で代入
            
            if humancount==1 or humancount==2: #人感センサ値が1か2のとき
                cnt+=1
                if cnt==2: #人感センサ値が２で出浴したとき
                    print("出浴した")
                    gudge=1 #グローバル関数に1を代入

        temp=float(dataTemp_bath[num-2]) #読み込んだ浴室温度をfloat型にして代入
        bathtemp=round(temp,1) #最新の浴室温度

        temp1=float(dataTemp_room[num-2]) #読み込んだ脱衣所温度をfloat型にして代入
        bathtemp1=round(temp1,1) #最新の脱衣所温度

        differ=round(bathtemp-bathtemp1,1) #浴室温度-脱衣所温度で温度差を求め四捨五入

        if bathtemp <= 23 and onoff == 0: #浴室23度以下かつ脱衣所暖房オフ
            print("湯沸かす前")
            nextcount = 0 
            ontime=0
            
        if onoff == 1 and humancount ==1 : #脱衣所暖房オンかつ人感センサ値１
            print("入浴開始")
            nothuman = 1
            

        if onoff == 1 and humancount ==2 :#脱衣所暖房オンかつ人感センサ値２
            print("入浴終了")
            nothuman=2
            result() #result関数で電源オフにする
            break
        
        if differ>=3 and onoff==0 and ontime==0: #温度差３度以上かつ脱衣所電源オフのとき
            print("暖房開始")
            nextcount=1
            ontime=2
            bathwarm=1
            result() #result関数で浴室暖房と脱衣所暖房の電源をオンにする
        
        if differ<=-3 and onoff==1 and state==0: #部屋>浴室で３度以上かつ脱衣所暖房オンかつ暖房モード強のとき
            print("弱暖にする")
            weakorder=2
            result() #result関数で脱衣所暖房の暖房モードを弱にする
        
        if differ>=5 and onoff==1 and state==2: #温度差５度以上かつ脱衣所暖房オンかつ暖房モード弱のとき
            print("強暖にする")
            strongorder+=1
            weakorder=0
            result() #result関数で暖房モードを強にする
        
        if bathtemp>=25 and bathwarm==1: #浴室温度２５度以上かつ浴室暖房オンのとき
            bathwarm=2
            print("浴室暖房終了")
            ifttt_webhook("bathroom") #IFTTTでbathroomのレシピを呼び出し浴室暖房OFF
        
    
    return 
    

def result():
    global nothuman #人がいるかどうか
    global onoff #脱衣所暖房の電源の状態（オフ0, オン1）
    global humancount #人感センサの値（人いない0, 人いる1）
    global nextcount #カウント
    global ontime #脱衣所暖房機がオンされたかどうか
    global weakorder #脱衣所暖房モード弱設定関数
    global state #脱衣所暖房機のモード(強暖０,弱１,弱暖2)
    global strongorder #脱衣所暖房モード強設定関数
    
    if onoff==1 and nothuman==2: #脱衣所暖房オフにする
        nothuman=0
        onoff=0
        humancount=0
        print("電源OFF")
        ifttt_webhook("switch") #IFTTTのswitchレシピを呼び出して脱衣所暖房の電源オフ      
    elif onoff==0 and nextcount==1 and ontime==2: #入浴開始前に浴室と脱衣所の暖房オンにする
        ontime=3
        onoff=1
        ifttt_webhook("switch") #IFTTTでswitchのレシピを呼び出し脱衣所暖房ON
        ifttt_webhook("bathroom") #IFTTTでbathroomのレシピを呼び出し浴室暖房ON
    elif onoff==1 and weakorder==2: #脱衣所暖房のモードを弱にする
        state=2
        print("mode変更")
        ifttt_webhook("mode") #IFTTTでmodeのレシピを呼び出し脱衣所暖房モード変更
        ifttt_webhook("mode")
    elif onoff==1 and strongorder==1: ##脱衣所暖房のモードを強にする
        strongorder=0
        state=0 
        weakorder=0
        print("mode変更")
        ifttt_webhook("mode") ##IFTTTでmodeのレシピを呼び出し脱衣所暖房モード変更

    return 
    

def main(): 
    
    loop = asyncio.get_event_loop()
    
    gather = asyncio.gather( #並行処理する関数
        excute(10), #温度差制御
        water(120), #湯温調節
        advice(120) #出浴アドバイス
    )
    
    loop.run_until_complete(gather)
    
 
if __name__ == '__main__':
    main()
