
//パッケージの読み込み許可
var express = require('express');
var ejs = require("ejs");
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

//ファイル読み込み許可
var fs = require('fs');
var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


//ブラウザ操作用のejsファイルのレダリング
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/images/', express.static(__dirname + 'safebath'));

app.post('/temp', function (req, res) { //温度データと人感センサ値を指定したファイルに書き込む
  fs.appendFile('/home/pi/safebathsub.csv', reqte.body.value1 + ',' + req.body.value2 + ',' + req.body.value3 + '\n',(error) => {
  }); 
});


//ローカルサーバーの起動
var server = app.listen(8000, function () { //ポート番号8000での通信
  var host = server.address().address
  var port = server.address().port
  console.log('This app listening at http://192.168.x.xx:',port)
});
